# search-app-frontend
![image](https://user-images.githubusercontent.com/26479/163654665-aefc0f97-29d2-4d5f-a6d6-11c47468a249.png)

Vue 3 Application https://vuejs.org/guide/introduction.html

##### Install dependencies
```shell
npm install
```

##### Start development server
```shell
vue serve
```

##### Run JS linter
```shell
vue lint
```

##### Build Docker Image
```shell
docker build . -f .gitlab/docker/nginx/Dockerfile -t nginx --no-cache
```
