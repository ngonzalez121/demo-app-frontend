#!/usr/bin/env bash

if [ ! -f 'node-v23.1.0-linux-arm64.tar.xz' ]; then

  wget https://nodejs.org/dist/v23.1.0/node-v23.1.0-linux-arm64.tar.xz
  mkdir -p ~/.nvm/versions/node/v23.1.0
  tar -xJf node-v23.1.0-linux-arm64.tar.xz -C ~/.nvm/versions/node/v23.1.0 --strip-components=1

fi

exit 0
