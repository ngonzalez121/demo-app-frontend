import { createRouter, createWebHistory } from 'vue-router'
import HomeShow from '../views/HomeShow.vue'
import DisplayItem from '../views/DisplayItem.vue'
import ImageFile from '../views/ImageFile.vue'
import AudioFile from '../views/AudioFile.vue'
import VideoFile from '../views/VideoFile.vue'
import TextFile from '../views/TextFile.vue'
import PdfFile from '../views/PdfFile.vue'
import NotFound from '../views/NotFound.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      redirect: '/home'
    },
    {
      path: '/home',
      name: 'home',
      component: HomeShow
    },
    {
      path: '/search/:q',
      name: 'home_search',
      component: HomeShow
    },
    {
      path: '/home/:folder',
      name: 'home_folder',
      component: HomeShow
    },
    {
      path: '/home/:folder/:subfolder',
      name: 'home_folder_subfolder',
      component: HomeShow
    },
    {
      path: '/home',
      name: 'home_redirect',
      component: HomeShow
    },
    {
      path: '/:id',
      name: 'show',
      component: DisplayItem
    },
    {
      path: '/:folderDataUrl/image/:imageFileDataUrl',
      name: 'ImageFile',
      component: ImageFile
    },
    {
      path: '/:folderDataUrl/audio/:audioFileDataUrl',
      name: 'AudioFile',
      component: AudioFile
    },
    {
      path: '/:folderDataUrl/video/:videoFileDataUrl',
      name: 'VideoFile',
      component: VideoFile
    },
    {
      path: '/:folderDataUrl/pdf/:pdfFileDataUrl',
      name: 'PdfFile',
      component: PdfFile
    },
    {
      path: '/:folderDataUrl/text/:textFileDataUrl',
      name: 'TextFile',
      component: TextFile
    },
    {
      path: '/:catchAll(.*)*',
      name: 'NotFound',
      component: NotFound
    }
  ]
})

export default router
