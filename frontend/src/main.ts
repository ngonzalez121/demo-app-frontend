// Vue
import { createApp, h } from 'vue'

// VueToastNotification
import VueToast from 'vue-toast-notification'

// Vuetify
// https://next.vuetifyjs.com/en/
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'

// Import CSS
import 'vuetify/styles'
import '@mdi/font/css/materialdesignicons.css'
import 'vue-toast-notification/dist/theme-sugar.css'
import 'vue3-pdf-app/dist/icons/main.css';
import './assets/base.css'
import './assets/bootstrap.scss'

// MDI - JS SVG
import { aliases, mdi } from 'vuetify/iconsets/mdi'

const Vuetify = createVuetify({
  components,
  directives,
  icons: {
    defaultSet: 'mdi',
    aliases,
    sets: {
      mdi,
    }
  },
  theme: {
    defaultTheme: 'dark'
  }
})

// Apollo
import { ApolloClient, HttpLink, InMemoryCache } from '@apollo/client/core'
import { createApolloProvider } from '@vue/apollo-option'
import VueApolloComponents from '@vue/apollo-components'

// GraphQL endpoint
const httpLink = new HttpLink({
  uri: 'https://link12.ddns.net:4040/graphql'
})

// Cache implementation
const cache = new InMemoryCache()

// Create the apollo client
const apolloClient = new ApolloClient({
  link: httpLink,
  cache,
  defaultOptions: {
    query: {
      fetchPolicy: 'no-cache'
    }
  }
})

// The provider holds the Apollo client instances that can then be used by all the child components.
const apolloProvider = createApolloProvider({
  defaultClient: apolloClient
})

// Router
import Router from './router/index'

// i18n
import { createI18n } from 'vue-i18n'
import messages from './locales/en.json'

const i18n = createI18n({
  locale: 'en',
  fallbackLocale: 'en',
  messages
})

// Pinia
import { createPinia } from 'pinia'
const pinia = createPinia()

// Configurable persistence and rehydration of Pinia stores.
// https://github.com/prazdevs/pinia-plugin-persistedstate
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'
pinia.use(piniaPluginPersistedstate)

// API for pinia for storage, encryption
// https://github.com/mioxs/pinia-plugin-store
import { storePlugin } from 'pinia-plugin-store'
import Utf8 from 'crypto-js/enc-utf8'
import Base64 from 'crypto-js/enc-base64'

function encrypt(value: string): string {
  return Base64.stringify(Utf8.parse(value));
}

function decrypt(value: string): string {
  return Base64.parse(value).toString(Utf8);
}

const plugin = storePlugin({
  stores: [{ name: 'main' }],
  encrypt,
  decrypt,
});

pinia.use(plugin);

// Application
import App from './App.vue'

const app = createApp({
  render: () => h(App)
})

app.use(pinia)
app.use(i18n)
app.use(Router)
app.use(apolloProvider)
app.use(VueApolloComponents)
app.use(Vuetify)
app.use(VueToast)

// Store Helpers
import StoreDataConfiguration from './configurations/store_data'
app.mixin(StoreDataConfiguration as any)

document.addEventListener('DOMContentLoaded', () => {
  app.mount('#app')
})
