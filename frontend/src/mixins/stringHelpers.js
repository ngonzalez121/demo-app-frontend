export default {
  methods: {
    truncate(text, limit, value) {
      if (text.length > limit) {
        let strLength1 = text.substring(0, text.length/2)
        let strLength2 = text.substring(text.length/2, text.length)
        return [
          strLength1.substring(0, strLength1.length-value),
          '...',
          strLength2.substring(value, strLength2.length),
        ].join('')
      } else {
        return text
      }
    },
  }
}
