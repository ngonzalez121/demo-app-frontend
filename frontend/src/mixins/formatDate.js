// moment
import moment from 'moment'

export default {
  methods: {
    isDate(dateString) {
      // 2024-09-16T17:57:07.000-05:00
      if (dateString.length == null || dateString.length < 10) return false
      return moment(dateString, moment.ISO_8601).isValid()
    },
    formatDate(dateString) {
      const date = new Date(dateString);
      return new Intl.DateTimeFormat('en-US', {
                   dateStyle: 'full',
                   timeStyle: 'long',
                   timeZone: 'Europe/Paris',
                 }).format(date)
    }
  }
}
