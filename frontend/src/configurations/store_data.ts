import { defineComponent } from 'vue';
import { mapStores } from 'pinia'
import { useStore } from '../stores/store'

export default defineComponent({
	computed: {
		...mapStores(useStore),
    searchQuery(): string {
      let store = this.mainStore.storeData as any;
      if (Object.keys(store.search).length == 0) { return '' }
      return store.search.q;
    },
    searchResults(): object[] {
      let store = this.mainStore.storeData as any;
      if (Object.keys(store.search.response).length == 0) { return [] }
      return store.search.response.results;
    },
    currentPage(): number {
      let store = this.mainStore.storeData as any;
      if (Object.keys(store.search.response).length == 0) { return 1 }
      return store.search.response.currentPage;
    },
    totalPages(): number {
      let store = this.mainStore.storeData as any;
      if (Object.keys(store.search.response).length == 0) { return 1 }
      return store.search.response.totalPages;
    },
    hasResults(): boolean {
      let store = this.mainStore.storeData as any;
      if (Object.keys(store.search.response).length == 0) { return false }
      return store.search.response.results.length > 0
    },
    /* Audio */
    audioFiles(): object[] {
      let store = this.mainStore.storeData as any;
      if (Object.keys(store.displayItem).length == 0) { return [] }
      return store.displayItem.audioFiles;
    },
    hasAudioFiles(): boolean {
      let store = this.mainStore.storeData as any;
      if (Object.keys(store.displayItem).length == 0) { return false }
      return store.displayItem.audioFiles.length > 0
    },
    /* Video */
    videoFiles(): object[] {
      let store = this.mainStore.storeData as any;
      if (Object.keys(store.displayItem).length == 0) { return [] }
      return store.displayItem.videoFiles;
    },
    hasVideoFiles(): boolean {
      let store = this.mainStore.storeData as any;
      if (Object.keys(store.displayItem).length == 0) { return false }
      return store.displayItem.videoFiles.length > 0
    },
    /* Image */
    imageFiles(): object[] {
      let store = this.mainStore.storeData as any;
      if (Object.keys(store.displayItem).length == 0) { return [] }
      return store.displayItem.imageFiles;
    },
    hasImageFiles(): boolean {
      let store = this.mainStore.storeData as any;
      if (Object.keys(store.displayItem).length == 0) { return false }
      return store.displayItem.imageFiles.length > 0
    },
    /* Pdf */
    pdfFiles(): object[] {
      let store = this.mainStore.storeData as any;
      if (Object.keys(store.displayItem).length == 0) { return [] }
      return store.displayItem.pdfFiles;
    },
    hasPdfFiles(): boolean {
      let store = this.mainStore.storeData as any;
      if (Object.keys(store.displayItem).length == 0) { return false }
      return store.displayItem.pdfFiles.length > 0
    },
    /* Text */
    textFiles(): object[] {
      let store = this.mainStore.storeData as any;
      if (Object.keys(store.displayItem).length == 0) { return [] }
      return store.displayItem.textFiles;
    },
    hasTextFiles(): boolean {
      let store = this.mainStore.storeData as any;
      if (Object.keys(store.displayItem).length == 0) { return false }
      return store.displayItem.textFiles.length > 0
    },
	}
})
