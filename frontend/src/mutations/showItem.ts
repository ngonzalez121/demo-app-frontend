import gql from 'graphql-tag'

const mutation = gql`
  mutation showItem($id: String!) {
    showItem(input: { id: $id }) {
      folder {
        attributes
      }
      audioFiles {
        attributes
      }
      videoFiles {
        attributes
      }
      imageFiles {
        attributes
      }
      pdfFiles {
        attributes
      }
      textFiles {
        attributes
      }
      loading
      errors
    }
  }
`

export default function ({ apollo, id }) {
  return apollo.mutate({
    mutation,
    variables: {
      id
    }
  })
}
