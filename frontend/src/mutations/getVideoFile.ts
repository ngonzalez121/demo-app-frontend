import gql from 'graphql-tag'

const mutation = gql`
  mutation getVideoFile($dataUrl: String!) {
    getVideoFile(input: { dataUrl: $dataUrl }) {
      videoFile {
        attributes
      }
      loading
      errors
    }
  }
`

export default function ({ apollo, dataUrl }) {
  return apollo.mutate({
    mutation,
    variables: {
      dataUrl
    }
  })
}
