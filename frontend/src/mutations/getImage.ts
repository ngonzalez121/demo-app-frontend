import gql from 'graphql-tag'

const mutation = gql`
  mutation getImage($dataUrl: String!) {
    getImage(input: { dataUrl: $dataUrl }) {
      image {
        attributes
      }
      loading
      errors
    }
  }
`

export default function ({ apollo, dataUrl }) {
  return apollo.mutate({
    mutation,
    variables: {
      dataUrl
    }
  })
}
