import gql from 'graphql-tag'

const mutation = gql`
  mutation getFolder($dataUrl: String!) {
    getFolder(input: { dataUrl: $dataUrl }) {
      folder {
        attributes
      }
      loading
      errors
    }
  }
`

export default function ({ apollo, dataUrl }) {
  return apollo.mutate({
    mutation,
    variables: {
      dataUrl
    }
  })
}
