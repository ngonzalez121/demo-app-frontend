import gql from 'graphql-tag'

const mutation = gql`
  mutation getText($dataUrl: String!) {
    getText(input: { dataUrl: $dataUrl }) {
      text {
        attributes
      }
      loading
      errors
    }
  }
`

export default function ({ apollo, dataUrl }) {
  return apollo.mutate({
    mutation,
    variables: {
      dataUrl
    }
  })
}
