import gql from 'graphql-tag'

const mutation = gql`
  mutation getAudioFile($dataUrl: String!) {
    getAudioFile(input: { dataUrl: $dataUrl }) {
      audioFile {
        attributes
      }
      loading
      errors
    }
  }
`

export default function ({ apollo, dataUrl }) {
  return apollo.mutate({
    mutation,
    variables: {
      dataUrl
    }
  })
}
