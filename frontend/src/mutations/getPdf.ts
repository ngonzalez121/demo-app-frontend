import gql from 'graphql-tag'

const mutation = gql`
  mutation getPdf($dataUrl: String!) {
    getPdf(input: { dataUrl: $dataUrl }) {
      pdf {
        attributes
      }
      loading
      errors
    }
  }
`

export default function ({ apollo, dataUrl }) {
  return apollo.mutate({
    mutation,
    variables: {
      dataUrl
    }
  })
}
