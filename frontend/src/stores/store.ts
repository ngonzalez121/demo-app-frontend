import { defineStore } from 'pinia'

export const useStore = defineStore('main', {
  state: () => {
    return {
      storeData: {
        search: {
          response: {
            //
          }
        },
        displayItem: {

        },
        carouselModel: {

        }
      },
    }
  },
  persist: {
    storage: sessionStorage,
    paths: ['storeData'],
  },
  actions: {
    setStoreData(data: any) {
      for (const k in data) {
        this.storeData[k] = data[k]
      }
    }
  }
})
